#!/usr/bin/make

.PHONY : help up down

.DEFAULT_GOAL := help

help:
	@echo "\n \
	*********************\n \
	*** Make commands ***\n \
	*********************\n\n \
	help	- Show this help\n \
	up	- Start backend containers (you can send param CONTAINER)\n \
	down	- Stop backend containers\n \
	shell	- Attach to container (send param CONTAINER like 'make shell CONTAINER=nginx')\n \
	logs	- Container logs (send param CONTAINER or FLAG) \
	"

up:
	@docker-compose -f docker-compose.yml up $(CONTAINER) -d

down:
	@docker-compose -f docker-compose.yml down

shell: up
	@docker-compose exec $(CONTAINER) /bin/sh

logs: up
	@docker-compose logs $(CONTAINER) $(FLAG)
