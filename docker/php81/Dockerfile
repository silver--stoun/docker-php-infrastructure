FROM php:8.1-fpm-alpine
LABEL Description="PHP 8.2 container"

ENV COMPOSER_VERSION 2.5.1
ENV PHP_XDEBUG_VERSION 3.2.0

RUN apk add --no-cache --virtual .persistent-deps \
    icu-dev \
    libintl gettext-dev libxslt \
    postgresql-dev \
    # etc
    bash vim git make zlib-dev

RUN set -xe \
    && apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        rabbitmq-c-dev \
    && mkdir -p /usr/src/php/ext/amqp \
    && curl -fsSL https://pecl.php.net/get/amqp | tar xvz -C "/usr/src/php/ext/amqp" --strip 1 \
    && docker-php-ext-configure intl --enable-intl \
    && docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-configure pdo_pgsql --with-pdo-pgsql \
    && docker-php-ext-install -j$(nproc) \
        intl \
        pcntl \
        pdo_pgsql \
        amqp \
    && apk add --update linux-headers \
    && pecl install xdebug-${PHP_XDEBUG_VERSION} \
    && docker-php-ext-enable xdebug \
    && rm -rf /scripts \
    && mkdir /scripts \
    && mkdir -p /projects/backend

COPY ./etc/php/php.ini /usr/local/etc/php/php.ini
COPY ./etc/php/php-fpm.conf /usr/local/etc/php-fpm.conf

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /tmp

RUN set -xe \
    && mkdir -p "$COMPOSER_HOME" \
    # install composer
    && curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php \
    && php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba' . \
    '75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else \
    { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php /tmp/composer-setup.php --no-ansi --install-dir=/usr/bin --filename=composer --version=$COMPOSER_VERSION \
    && composer clear-cache \
    && rm -rf /tmp/composer-setup.php /tmp/.htaccess \
    # show php info
    && php -v \
    && php-fpm -v \
    && php -m

RUN cd /usr/lib && git clone https://github.com/NoiseByNorthwest/php-spx.git \
    && cd /usr/lib/php-spx \
    && phpize \
    && ./configure \
    && make \
    && make install


COPY ./keep-alive.sh /scripts/keep-alive.sh
COPY ./fpm-entrypoint.sh /fpm-entrypoint.sh

WORKDIR /projects/backend
ENTRYPOINT []
CMD []

