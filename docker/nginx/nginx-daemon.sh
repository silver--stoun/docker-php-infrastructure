#!/usr/bin/env sh

getProjects() {
    cd /projects/backend
    unset backend_projects backend_project_templates
    backend_projects=$(find . -type f -name 'nginx.template' -exec dirname {} \; | sed 's/\/.nginx//' | cut -d '/' -f2- | sort -u)

    echo -e "Found next projects:\n"
    echo -e "--------------------\n"
    for i in $backend_projects; do
        backend_project_templates=$(echo $backend_project_templates /projects/backend/$i/.nginx/nginx.template )
        echo -e "* $i"
    done
    echo -e "--------------------\n"

    echo -e "Found next templates:\n"
    echo -e "---------------------\n"
    for i in $backend_project_templates; do
        echo -e "* $i"
    done
    echo -e "---------------------\n"
}

generateConfig() {
    for project in $backend_projects
    do
        cp /projects/backend/$project/.nginx/nginx.template /etc/nginx/conf.d/ms_$project.conf
    done
}

checksum() {
    for i in $backend_project_templates
    do
        md5sum $i | md5sum | awk '{print $1}'
    done
}

echo -e "[ NGINX DAEMON ] \n STARTED"
sh /nginx-entrypoint.sh

getProjects
generateConfig
checksum_init=$(checksum)

if nginx -t ; then
    echo "nginx starting..."
    nginx
    echo "nginx started"
else
    echo -e "Something wrong with nginx configuration\n"
fi

while true
do
    # getProjects
    # checksum_now=$(checksum)

    # if [[ $checksum_init != $checksum_now ]]; then
    #     echo -e "[ NGINX DAEMON ] \n A configuration files changed. Reloading..."
    #     generate_configs
    #     if nginx -t ; then
    #         echo "nginx starting..."
    #         nginx -s reload
    #         echo "nginx started"
    #     else
    #         echo "Something wrong with nginx configuration\n"
    #     fi
    #     unset checksum_init
    #     checksum_init=$(checksum)
    # fi

    sleep 2
done
