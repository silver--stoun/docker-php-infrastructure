#!/usr/bin/env sh
set -e

SERVER_CONFIG_PATH="${SERVER_CONFIG_PATH:-/etc/nginx/nginx.conf}";

ROOT_DIR="${ROOT_DIR:-/usr/share/nginx/html}";
APP_BASE_URL="${APP_BASE_URL:-http://myapp.com/}";

sed -i "s#%ROOT_DIR_FRONTEND%#${ROOT_DIR_FRONTEND}#g" "$SERVER_CONFIG_PATH";
sed -i "s#%APP_BASE_URL%#${APP_BASE_URL}#g" "$SERVER_CONFIG_PATH";

exec "$@";
